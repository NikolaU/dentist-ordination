CREATE DATABASE  IF NOT EXISTS `zubar` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `zubar`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: localhost    Database: zubar
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `acc_id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_role` varchar(45) DEFAULT NULL,
  `acc_description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`acc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'Admin','Admin celog sistema'),(2,'Pacijent','Pacijent koji je registrovan na sistemu'),(3,'Doktor','Doktor koji je zaposlen u ordinaciji');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_first_name` varchar(45) NOT NULL,
  `doc_last_name` varchar(45) NOT NULL,
  `doc_pass` varchar(45) NOT NULL,
  `doc_gender` varchar(45) DEFAULT NULL,
  `doc_phone` varchar(45) DEFAULT NULL,
  `doc_email` varchar(45) DEFAULT NULL,
  `doc_address` varchar(45) DEFAULT NULL,
  `doc_acc_id` int(11) NOT NULL,
  PRIMARY KEY (`doc_id`),
  KEY `fk_doc_acc_id` (`doc_acc_id`),
  CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`doc_acc_id`) REFERENCES `account` (`acc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,'Nikola','Uzelac','password','Muski','254145','uzelac@yahoo.com','Savska',3),(3,'Jovana','Jovic','password','Zenski','354-845-9658','jjovic','Visegradska',3),(4,'Milica','Milic','password','Zenski','888-9999','mmilic@yahoo.com','Gandijeva',3);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `pat_id` int(11) NOT NULL AUTO_INCREMENT,
  `pat_first_name` varchar(45) NOT NULL,
  `pat_last_name` varchar(45) NOT NULL,
  `pat_gender` varchar(45) DEFAULT NULL,
  `pat_date_of_birth` varchar(10) DEFAULT NULL,
  `pat_phone` varchar(45) NOT NULL,
  `pat_email` varchar(45) DEFAULT NULL,
  `pat_address` varchar(100) DEFAULT NULL,
  `pat_acc_id` int(11) DEFAULT NULL,
  `pat_pass` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`pat_id`),
  KEY `fk_pat_acc_id` (`pat_acc_id`),
  CONSTRAINT `patient_ibfk_1` FOREIGN KEY (`pat_acc_id`) REFERENCES `account` (`acc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'Nikola','Pekovic','Muski','2008-11-11','354178','nikola@yahoo.com','Visegradska',2,'password'),(2,'Nikola','Uzelac','Muski','1994-12-18','064/1444-385','uzelac@yahoo.com','Savska',2,'password'),(3,'Jovana','Jovic','Zenski','1996-11-05','064/9896-222','jjovic@yahoo.com','Surdulicka',2,'password'),(4,'Milana','Popov','Zenski','1983-05-11','064/9896-222','mpopov@gmail.com','Dunavska',2,'password'),(5,'Tamara','Tatic','Zenski','31/08/2017','063/777-888','ttatic@gmail.com','Nehruova',2,'password');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `record`
--

DROP TABLE IF EXISTS `record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `record` (
  `rec_id` int(11) NOT NULL AUTO_INCREMENT,
  `rec_service` varchar(100) NOT NULL,
  `rec_description` varchar(150) DEFAULT NULL,
  `rec_pat_id` int(11) NOT NULL,
  PRIMARY KEY (`rec_id`),
  KEY `fk_rec_pat_id` (`rec_pat_id`),
  CONSTRAINT `record_ibfk_1` FOREIGN KEY (`rec_pat_id`) REFERENCES `patient` (`pat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `record`
--

LOCK TABLES `record` WRITE;
/*!40000 ALTER TABLE `record` DISABLE KEYS */;
/*!40000 ALTER TABLE `record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation` (
  `res_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_service` varchar(100) NOT NULL,
  `res_date` varchar(10) DEFAULT NULL,
  `res_time` varchar(10) NOT NULL,
  `res_doc_id` int(11) NOT NULL,
  `res_pat_id` int(11) NOT NULL,
  PRIMARY KEY (`res_id`),
  KEY `fk_res_doc_id` (`res_doc_id`),
  KEY `fk_res_pat_id` (`res_pat_id`),
  CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`res_doc_id`) REFERENCES `doctor` (`doc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`res_pat_id`) REFERENCES `patient` (`pat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (1,'Izbeljivanje zuba','18/08/2017','18:18',1,1),(2,'Skidanje kamenca','27/08/2017','15:15',4,1),(3,'Pregled','29/08/2017','14:10',4,1),(4,'Popravka zuba','03/08/2017','17:05',1,5),(5,'Skidanje kamenca','31/08/2017','15:15',4,1);
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-29 22:46:32
