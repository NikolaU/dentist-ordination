<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="all"
              href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/smoothness/jquery-ui.css"/>
        <!--        <link rel="stylesheet" href="/resources/demos/style.css">      
                <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
        <link href="css/custom.css" rel='stylesheet' type='text/css' />
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="dist/bootstrap-clockpicker.css">
        <title>Stomatološka ordinacija</title>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-ex-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp"><img id='logo' src="images/images.jpg" alt="stomatoloska ordinacija"></a>
                </div>
                <!-- MENI TABOVI -->
                <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li id="tab_login">
                            <a href="login.jsp"><i class="fa fa-sign-in" aria-hidden="true"></i> Prijava</a>
                        </li>
                        <li id="tab_registration">
                            <a href="registration.jsp"><i class="fa fa-user" aria-hidden="true"></i> Registracija</a>
                        </li>
                        <li id="tab_doctor" class="hidden">
                            <a href="doctor.jsp">Doktor panel <i class="fa fa-user-md" aria-hidden="true"></i></a>
                        </li>
                        <li id="tab_patient" class="hidden">
                            <a href="patient.jsp">Pacijent panel <i class="fa fa-user" aria-hidden="true"></i></a>
                        </li>
                        <li id="tab_logout" class="hidden">
                            <a href="LogoutServlet">Odjava <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="index.jsp">Početna</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">O nama<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="usluge.jsp">Usluge</a>
                        </li>
                        <li id="tab_reservation">
                            <a href="ReservationServlet">Rezervacija</a>
                        </li>
                        <li>
                            <a href="kontakt.jsp">Kontakt</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- NASLOV -->
        <div class="section rez-termina">
            <div class="container">
                <div class="row">
                    <h2>REZERVACIJA TERMINA</h2>
                    <br/>
                </div>
            </div>
        </div>

        <!-- KALENDAR -->
        <div class="section kalendar" style="background-size: cover;">
            <div class="container-fluid">
                <div class="row">
                    <div class="container" style="text-align: center;">
                        <br>
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">TERMIN</button>

                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Unos nove rezervacije</h4>
                                    </div>

                                    <!-- MODAL BODY-->
                                    <div class="modal-body">

                                        <form action="ReservationServlet" method="POST">

                                            <!-- KALENDAR -->
                                            <label for="datepicker">Datum:</label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" id="datepicker" placeholder="Unesite datum"
                                                           class="form-control" required="true" name="choose-date">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" id="datepicker"></span>
                                                    </span>
                                                </div>
                                            </div>

                                            <!-- SAT -->
                                            <label for="choose-time">Vreme:</label>
                                            <div class="form-group">
                                                <div class="input-group clockpicker" data-placement="left"
                                                     data-align="top" data-autoclose="true">

                                                    <input type="text" class="form-control"
                                                           id="choose-time" name="choose-time" required="true">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>

                                            <!-- VRSTA USLUGE -->
                                            <div class="form-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <label for="choose-service">Vrsta usluge:</label>
                                                    <select class="form-control" id="choose-service" style="border-radius: 3%;"
                                                            name="choose-service">
                                                        <option id="service1"></option>
                                                        <option id="service2"></option>
                                                        <option id="service3"></option>
                                                        <option id="service4"></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <!-- DOKTOR -->
                                            <div class="form-group">
                                                <div class="input-group" style="width: 100%;">
                                                    <label for="choose-doctor">Doktor:</label>
                                                    <select class="form-control" id="choose-doctor" style="border-radius: 3%;"
                                                            name="choose-doctor">
                                                        <c:forEach items="${requestScope.listDoctors}" var="listDoctors" >
                                                            <option><c:out value="${listDoctors.firstName} ${listDoctors.lastName} "/></option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-info btn-lg">REZERVIŠI</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        </div>

        <div class="section footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 h2-color">
                        <h2>STOMATOLOŠKA ORDINACIJA</h2>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> Savska 32</p>
                        <p><i class="fa fa-globe" aria-hidden="true"></i> 11000 Beograd, Srbija</p>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> Br. telefona: 064/4444-555</p>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail: stomord@gmail.com</p>
                    </div>
                    <div class="col-md-4 btn-klasa">
                        <button class="btn btn-lg">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-lg">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-lg">
                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script type="text/javascript" src="dist/bootstrap-clockpicker.min.js"></script>
        <script>
            window.onload = listServices();
            //lista usluga
            function listServices() {

                document.getElementById('service1').innerHTML = "Izbeljivanje zuba";
                document.getElementById('service2').innerHTML = "Popravka zuba";
                document.getElementById('service3').innerHTML = "Pregled";
                document.getElementById('service4').innerHTML = "Skidanje kamenca";

            }
            
            var provera = "${user}";
            if (provera == 2) {
                console.log("PACIJENT");
                document.getElementById('tab_login').setAttribute('class', 'hidden');
                document.getElementById('tab_registration').setAttribute('class', 'hidden');
                document.getElementById('tab_patient').removeAttribute('class', 'hidden');
                document.getElementById('tab_logout').removeAttribute('class', 'hidden');
            } else if (provera == 3) {
                console.log("DOKTOR");
                document.getElementById('tab_login').setAttribute('class', 'hidden');
                document.getElementById('tab_registration').setAttribute('class', 'hidden');
                document.getElementById('tab_reservation').setAttribute('class', 'hidden');
                document.getElementById('tab_doctor').removeAttribute('class', 'hidden');
                document.getElementById('tab_logout').removeAttribute('class', 'hidden');
            } else {
                console.log("OBICAN USER");
            }
        </script>

    </body>
</html>
