<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
        <link href="css/custom.css" rel='stylesheet' type='text/css' />
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <title>Stomatološka ordinacija</title>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-ex-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp"><img id='logo' src="images/images.jpg" alt="stomatoloska ordinacija"></a>
                </div>
                <!-- MENI TABOVI -->
                <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li id="tab_login">
                            <a href="login.jsp"><i class="fa fa-sign-in" aria-hidden="true"></i> Prijava</a>
                        </li>
                        <li id="tab_registration">
                            <a href="registration.jsp"><i class="fa fa-user" aria-hidden="true"></i> Registracija</a>
                        </li>
                        <li id="tab_doctor" class="hidden">
                            <a href="doctor.jsp">Doktor panel <i class="fa fa-user-md" aria-hidden="true"></i></a>
                        </li>
                        <li id="tab_patient" class="hidden">
                            <a href="patient.jsp">Pacijent panel <i class="fa fa-user" aria-hidden="true"></i></a>
                        </li>
                        <li id="tab_logout" class="hidden">
                            <a href="LogoutServlet">Odjava <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="index.jsp">Početna</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">O nama<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="usluge.jsp">Usluge</a>
                        </li>
                        <li id="tab_reservation">
                            <a href="ReservationServlet">Rezervacija</a>
                        </li>
                        <li>
                            <a href="kontakt.jsp">Kontakt</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- MAP -->
        <div id="map"></div>
        <script>
            function initMap() {
                var uluru = {lat: 44.803847, lng: 20.452990};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 10,
                    center: uluru
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBm3iDPg5LVEbo0rQJvVahA1deKIgh3u_o&callback=initMap">
        </script>

        <div class="section kontakt-klasa">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3>KONTAKT</h3>
                        <br/>
                        <h4><i class="fa fa-map-marker" aria-hidden="true"></i> Adresa:</h4>
                        <p>Savska 32, Beograd 11000, Srbija</p>
                        <br/>
                        <h4><i class="fa fa-phone" aria-hidden="true"></i> Broj telefona:</h4>
                        <p>064/4444-555</p>
                        <br/>
                        <h4><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail adresa:</h4>
                        <p>stomord@gmail.com</p>
                        <br/>
                        <h4><i class="fa fa-clock-o" aria-hidden="true"></i> Radno vreme:</h4>
                        <p>Ponedeljak-petak: 08-20h</p>
                        <p>Subota: 08-14h</p>
                        <br/>
                    </div>
                    <div class="col-md-6 form-klasa">
                        <div class="form-group">
                            <label for="name">IME:</label>
                            <input type="text" class="form-control" id="name" placeholder="Unesite Vaše ime">
                        </div>
                        <div class="form-group">
                            <label for="name">*VAŠ E-MAIL:</label>
                            <input type="text" class="form-control" id="name" placeholder="Unesite Vašu e-mail adresu">
                        </div>
                        <div class="form-group">
                            <label for="name">NASLOV:</label>
                            <input type="text" class="form-control" id="name" placeholder="Unesite naslov poruke">
                        </div>
                        <div class="form-group">
                            <label for="name">*PORUKA:</label>
                            <input type="text" class="form-control" id="name" placeholder="Unesite Vašu poruku">
                        </div>
                        <button class="btn btn-lg btn-info">
                            <p>POŠALJI <i class="fa fa-paper-plane" aria-hidden="true"></i></p>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <div class="section footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 h2-color">
                        <h2>STOMATOLOŠKA ORDINACIJA</h2>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> Savska 32</p>
                        <p><i class="fa fa-globe" aria-hidden="true"></i> 11000 Beograd, Srbija</p>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> Br. telefona: 064/4444-555</p>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail: stomord@gmail.com</p>
                    </div>
                    <div class="col-md-4 btn-klasa">
                        <button class="btn btn-lg">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-lg">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-lg">
                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script>

            var provera = "${user}";
            if (provera == 2) {
                console.log("PACIJENT");
                document.getElementById('tab_login').setAttribute('class', 'hidden');
                document.getElementById('tab_registration').setAttribute('class', 'hidden');
                document.getElementById('tab_patient').removeAttribute('class', 'hidden');
                document.getElementById('tab_logout').removeAttribute('class', 'hidden');
            } else if (provera == 3) {
                console.log("DOKTOR");
                document.getElementById('tab_login').setAttribute('class', 'hidden');
                document.getElementById('tab_registration').setAttribute('class', 'hidden');
                document.getElementById('tab_reservation').setAttribute('class', 'hidden');
                document.getElementById('tab_doctor').removeAttribute('class', 'hidden');
                document.getElementById('tab_logout').removeAttribute('class', 'hidden');
            } else {
                console.log("OBICAN USER");
            }
        </script>
        
    </body>
</html>
