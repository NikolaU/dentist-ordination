<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
        <link href="css/custom.css" rel='stylesheet' type='text/css' />
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <title>Stomatološka ordinacija</title>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-ex-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp"><img id='logo' src="images/images.jpg" alt="stomatoloska ordinacija"></a>
                </div>
                <!-- MENI TABOVI -->
                <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li id="tab_login">
                            <a href="login.jsp"><i class="fa fa-sign-in" aria-hidden="true"></i> Prijava</a>
                        </li>
                        <li id="tab_registration">
                            <a href="registration.jsp"><i class="fa fa-user" aria-hidden="true"></i> Registracija</a>
                        </li>
                        <li id="tab_doctor" class="hidden">
                            <a href="doctor.jsp">Doktor panel <i class="fa fa-user-md" aria-hidden="true"></i></a>
                        </li>
                        <li id="tab_patient" class="hidden">
                            <a href="patient.jsp">Pacijent panel <i class="fa fa-user" aria-hidden="true"></i></a>
                        </li>
                        <li id="tab_logout" class="hidden">
                            <a href="LogoutServlet">Odjava <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="index.jsp">Početna</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">O nama<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="usluge.jsp">Usluge</a>
                        </li>
                        <li id="tab_reservation">
                            <a href="ReservationServlet">Rezervacija</a>
                        </li>
                        <li>
                            <a href="kontakt.jsp">Kontakt</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="section img-usluge">
            <div class="container-fluid">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <h2 class="text-color">USLUGE</h2>
                            <p class="text-color">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section usluge-ponuda">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="feature">
                            <div class="feature-icon"><i class="fa fa-heart" aria-hidden="true"></i></div> 
                            <strong class="feature-title">Skidanje karijesa</strong>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
                                architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature">
                            <div class="feature-icon"><i class="fa fa-heart" aria-hidden="true"></i></div> 
                            <strong class="feature-title">Pregled</strong>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
                                architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature">
                            <div class="feature-icon"><i class="fa fa-heart" aria-hidden="true"></i></div> 
                            <strong class="feature-title">Izbeljivanje zuba</strong>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
                                architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="feature">
                            <div class="feature-icon"><i class="fa fa-user-md" aria-hidden="true"></i></div> 
                            <strong class="feature-title">Zubni mostovi</strong>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
                                architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature">
                            <div class="feature-icon"><i class="fa fa-user-md" aria-hidden="true"></i></div> 
                            <strong class="feature-title">Protetika rascepa</strong>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
                                architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature">
                            <div class="feature-icon"><i class="fa fa-user-md" aria-hidden="true"></i></div> 
                            <strong class="feature-title">Dentalni nakit</strong>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
                                architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/><br/>
        <div class="section footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 h2-color">
                        <h2>STOMATOLOŠKA ORDINACIJA</h2>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> Savska 32</p>
                        <p><i class="fa fa-globe" aria-hidden="true"></i> 11000 Beograd, Srbija</p>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> Br. telefona: 064/4444-555</p>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail: stomord@gmail.com</p>
                    </div>
                    <div class="col-md-4 btn-klasa">
                        <button class="btn btn-lg">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-lg">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-lg">
                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script>

            var provera = "${user}";
            if (provera == 2) {
                console.log("PACIJENT");
                document.getElementById('tab_login').setAttribute('class', 'hidden');
                document.getElementById('tab_registration').setAttribute('class', 'hidden');
                document.getElementById('tab_patient').removeAttribute('class', 'hidden');
                document.getElementById('tab_logout').removeAttribute('class', 'hidden');
            } else if (provera == 3) {
                console.log("DOKTOR");
                document.getElementById('tab_login').setAttribute('class', 'hidden');
                document.getElementById('tab_registration').setAttribute('class', 'hidden');
                document.getElementById('tab_reservation').setAttribute('class', 'hidden');
                document.getElementById('tab_doctor').removeAttribute('class', 'hidden');
                document.getElementById('tab_logout').removeAttribute('class', 'hidden');
            } else {
                console.log("OBICAN USER");
            }
        </script>
        
    </body>
</html>
