<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css'/>
        <link href="css/custom.css" rel='stylesheet' type='text/css' />
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <title>Stomatološka ordinacija</title>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-ex-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp"><img id='logo' src="images/images.jpg" alt="stomatoloska ordinacija"></a>
                </div>
                <!-- MENI TABOVI -->
                <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li id="tab_login">
                            <a href="login.jsp"><i class="fa fa-sign-in" aria-hidden="true"></i> Prijava</a>
                        </li>
                        <li id="tab_registration">
                            <a href="registration.jsp"><i class="fa fa-user" aria-hidden="true"></i> Registracija</a>
                        </li>
                        <li id="tab_doctor" class="hidden">
                            <a href="doctor.jsp">Doktor panel <i class="fa fa-user-md" aria-hidden="true"></i></a>
                        </li>
                        <li id="tab_patient" class="hidden">
                            <a href="patient.jsp">Pacijent panel <i class="fa fa-user" aria-hidden="true"></i></a>
                        </li>
                        <li id="tab_logout" class="hidden">
                            <a href="LogoutServlet">Odjava <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="index.jsp">Početna</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">O nama<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                                <li><a href="#">O nama</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="usluge.jsp">Usluge</a>
                        </li>
                        <li id="tab_reservation">
                            <a href="ReservationServlet">Rezervacija</a>
                        </li>
                        <li>
                            <a href="kontakt.jsp">Kontakt</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- SLIDESHOW -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img class="carouselPic" src="images/chair.jpg" alt="Dentist chair">
                </div>

                <div class="item">
                    <img class="carouselPic" src="images/proteza.jpg" alt="Proteza">
                </div>

                <div class="item">
                    <img class="carouselPic" src="images/chair.jpg" alt="Dentist chair">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="usluge">
            <h2>
                <br/>USLUGE
            </h2>
        </div>

        <div class="section" style="background-color: #5bc0de;">
            <div class="container">
                <br/>
                <div class="row">
                    <div class="col-md-4 image-size">
                        <img src="images/pregled.jpg" alt="Pregled" 
                             class="center-block img-thumbnail img-responsive" >
                        <h3 class="text-center" style="color: white;">Pregled</h3>
                    </div>
                    <div class="col-md-4 image-size">
                        <img src="images/izbeljivanje.jpg" alt="Izbeljivanje zuba" 
                             class="center-block img-thumbnail img-responsive" >
                        <h3 class="text-center" style="color: white;">Izbeljivanje zuba</h3>
                    </div>
                    <div class="col-md-4 image-size">
                        <img src="images/karijes.jpg" alt="Skidanje karijesa" 
                             class="center-block img-thumbnail img-responsive" >
                        <h3 class="text-center" style="color: white;">Skidanje karijesa</h3>
                    </div>
                </div>
            </div>    
        </div>    

        <div class="section" style="background-color: #ebebeb;">
            <div class="container">
                <div class="row pocetna-usluge">
                    <h2>
                        <br/>ZADOVOLJAN I NASMEJAN PACIJENT JE NAŠ PRIMARNI CILJ!
                    </h2>
                    <br/>
                    <p>Kada Vam nudimo tretman, mi uzimamo u obzir Vaše želje. 
                        Objašnjavamo Vam koje Vam mogućnosti stoje na raspolaganju, njihovu cenu, tako da možete napraviti izbor. 
                        Uvek ćete znati šta radimo. U svakom trenutku, ako imate neko pitanje ili niste sigurni šta se događa, 
                        slobodno pitajte nekog od članova našeg tima stomatologa koji će biti srećni da Vam sve objasne.
                        Kao praksa, mi stvarno verujemo da su preventivna nega i obrazovanje put do optimalnog zdravlja zuba. 
                        Nastojimo da obezbedimo „zdravstvenu negu zuba".
                        <br/><br/>Zato se, kroz razgovor sa pacijentom, 
                        fokusiramo na celokupno stanje usta, zuba i desni. Naš cilj je dobro zdravlje zuba za Vas i Vašu porodicu 
                        kroz prevenciju. To znači da Vam dajemo savete, pomažemo Vam da razumete kako da se brinete o svojim zubima
                        da bi se smanjila potreba za stomatološkim tretmanima. Mi Vas savetujemo da redovno obavljate rutinsko 
                        uklanjanje kamenca i mekih naslaga, čišćenje koncem, fluorisanje jer to sve pomaže u prevenciji zubnih 
                        bolesti. Nismo fokusirani samo na Vaš predivan osmeh, takođe se brinemo i za Vaše zdravlje.
                        <br/><br/>
                        Veselimo se Vašem dolasku!
                    </p>
                </div>
            </div>
        </div>

        <div class="section reservation-part">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 image-header">
                        <h2 class="over-image">REZERVIŠITE VAŠ TERMIN</h2>
                    </div>
                    <div class="col-md-4 btn-header">
                        <button class="btn btn-info btn-lg" onclick="location.href = 'ReservationServlet'">REZERVISI</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- FOOTER -->

        <div class="section footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 h2-color">
                        <h2>STOMATOLOŠKA ORDINACIJA</h2>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> Savska 32</p>
                        <p><i class="fa fa-globe" aria-hidden="true"></i> 11000 Beograd, Srbija</p>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> Br. telefona: 064/4444-555</p>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail: stomord@gmail.com</p>
                    </div>
                    <div class="col-md-4 btn-klasa">
                        <button class="btn btn-lg">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-lg">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-lg">
                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script>

            var provera = "${user}";
            if (provera == 2) {
                console.log("PACIJENT");
                document.getElementById('tab_login').setAttribute('class', 'hidden');
                document.getElementById('tab_registration').setAttribute('class', 'hidden');
                document.getElementById('tab_patient').removeAttribute('class', 'hidden');
                document.getElementById('tab_logout').removeAttribute('class', 'hidden');
            } else if (provera == 3) {
                console.log("DOKTOR");
                document.getElementById('tab_login').setAttribute('class', 'hidden');
                document.getElementById('tab_registration').setAttribute('class', 'hidden');
                document.getElementById('tab_reservation').setAttribute('class', 'hidden');
                document.getElementById('tab_doctor').removeAttribute('class', 'hidden');
                document.getElementById('tab_logout').removeAttribute('class', 'hidden');
            } else {
                console.log("OBICAN USER");
            }
        </script>
    </body>
</html>