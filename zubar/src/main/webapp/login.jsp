<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel='stylesheet' type='text/css' />
        <title>Stomatolo�ka ordinacija</title>
    </head>
    <body>

        <br>
        <div class="container-fluid">
            <div class="panel panel-success">
                <div class="panel-heading" align="center" style="background-color: #5bc0de;">
                    <h4>
                        <b><font color="white" style="font-weight: 900;">PRIJAVA</font> </b>
                    </h4>
                </div>
                <div class="panel-body" align="center">

                    <div class="container " style="margin-top: 10%; margin-bottom: 10%;">

                        <div class="panel panel-success" style="max-width: 35%; border-color: #5bc0de;"
                             align="left">

                            <div class="panel-heading form-group" style="background-color: #5bc0de;">
                                <b><font color="white"> Prijava</font></b>
                            </div>

                            <div class="panel-body">

                                <form action="LoginServlet" method="post">
                                    
                                    <!-- EMAIL -->
                                    <div class="form-group">
                                        <label for="txtEmail">Email</label>
                                        <input
                                            type="text" class="form-control" name="txtEmail"
                                            id="txtEmail" placeholder="Email"
                                            required="required">
                                    </div>
                                    
                                    <!-- SIFRA -->
                                    <div class="form-group">
                                        <label for="txtPass">�ifra</label> 
                                        <input
                                            type="password" class="form-control" name="txtPass"
                                            id="txtPass" placeholder="�ifra" required="required">
                                    </div>
                                    
                                    <button type="submit" style="width: 100%; font-size: 1.1em;"
                                            class="btn btn-large btn btn-success btn-lg btn-block">
                                        <b>PRIJAVA</b>
                                    </button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" align="center" style="background-color: #5bc0de;">
                    <font style="color: white; font-weight: 900;">STOMATOLO�KA ORDINACIJA</font>
                </div>
            </div>
        </div>
    </body>
</html>
