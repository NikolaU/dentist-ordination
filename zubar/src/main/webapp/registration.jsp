<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" media="all"
              href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/smoothness/jquery-ui.css"/>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="dist/bootstrap-clockpicker.css">
        <title>Stomatološka ordinacija</title>
    </head>
    <body>

        <br>
        <div class="container-fluid">
            <div class="panel panel-success">
                <div class="panel-heading" align="center" style="background-color: #5bc0de;">
                    <h4>
                        <b><font color="white" style="font-weight: 900;">REGISTRACIJA</font> </b>
                    </h4>
                </div>
                <div class="panel-body" align="center">

                    <div class="container " style="margin-top: 10%; margin-bottom: 10%;">

                        <div class="panel panel-success" style="max-width: 35%; border-color: #5bc0de;"
                             align="left">

                            <div class="panel-heading form-group" style="background-color: #5bc0de;">
                                <b><font color="white"> Registracija</font></b>
                            </div>

                            <div class="panel-body">

                                <form action="RegistrationServlet" method="put">
                                    
                                    <div class="form-group">
                                        <label for="namePat">Ime*</label>
                                        <input type="text" class="form-control" name="namePat"
                                               id="namePat" placeholder="Ime"
                                               required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="surnamePat">Prezime*</label>
                                        <input type="text" class="form-control" name="surnamePat"
                                               id="surnamePat" placeholder="Prezime"
                                               required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="emailPat">Email*</label>
                                        <input
                                            type="text" class="form-control" name="emailPat"
                                            id="emailPat" placeholder="Email"
                                            required="required">
                                    </div>
                                     <div class="form-group">
                                        <label for="passPat">Šifra*</label> 
                                        <input
                                            type="password" class="form-control" name="passPat"
                                            id="passPat" placeholder="Šifra" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="genderPat">Pol</label>
                                        <input type="text" class="form-control" name="genderPat"
                                               id="genderPat" placeholder="Pol">
                                    </div>
                                    <label for="datepicker">Datum rodjenja:</label>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" id="datepicker" placeholder="Unesite datum"
                                                        class="form-control" required="true" name="datePat">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" id="datepicker"></span>
                                                </span>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <label for="telPat">Broj telefona*</label>
                                        <input type="text" class="form-control" name="telPat"
                                               id="telPat" placeholder="Broj telefona"
                                               required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="addressPat">Adresa</label>
                                        <input type="text" class="form-control" name="addressPat"
                                               id="addressPat" placeholder="Broj telefona">
                                    </div>
                                    <button type="submit" style="width: 100%; font-size: 1.1em;"
                                            class="btn btn-large btn btn-success btn-lg btn-block">
                                        <b>REGISTRACIJA</b>
                                    </button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" align="center" style="background-color: #5bc0de;">
                    <font style="color: white; font-weight: 900;">STOMATOLOŠKA ORDINACIJA</font>
                </div>
            </div>
        </div>
        
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
        <script type="text/javascript" src="dist/bootstrap-clockpicker.min.js"></script>
        
    </body>
</html>
