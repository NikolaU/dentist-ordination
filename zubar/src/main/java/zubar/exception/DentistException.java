package zubar.exception;

public class DentistException extends Exception{

    public DentistException() {
    }
    
    public DentistException(String message){
        super(message);
    }
    
    public DentistException(String message, Exception e){
        super(message, e);
    }
    
}
