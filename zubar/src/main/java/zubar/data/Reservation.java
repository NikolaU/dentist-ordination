package zubar.data;

import java.sql.Date;

public class Reservation {

    private int id;
    private String service;
    private String date;
    private String time;
    private Doctor docId;
    private Patient patId;

    public Reservation() {
    }

    public Reservation(int id, String service, String date, String time, Doctor docId, Patient patId) {
        this.id = id;
        this.service = service;
        this.date = date;
        this.time = time;
        this.docId = docId;
        this.patId = patId;
    }
    
    public Reservation(String service, String date, String time, Doctor docId, Patient patId){
        this.service = service;
        this.date = date;
        this.time = time;
        this.docId = docId;
        this.patId = patId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Doctor getDocId() {
        return docId;
    }

    public void setDocId(Doctor docId) {
        this.docId = docId;
    }

    public Patient getPatId() {
        return patId;
    }

    public void setPatId(Patient patId) {
        this.patId = patId;
    }

    @Override
    public String toString() {
        return "Reservation{" + "id=" + id + ", service=" + service + ", date=" + date + ", time=" + time + ", docId=" + docId + ", patId=" + patId + '}';
    }
    
}