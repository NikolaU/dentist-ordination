package zubar.data;

public class Record {

    private int id;
    private String service;
    private String description;
    private Patient patId;

    public Record() {
    }

    public Record(int id, String service, String description, Patient patId) {
        this.id = id;
        this.service = service;
        this.description = description;
        this.patId = patId;
    }
    
    public Record(String service, String description, Patient patId){
        this.service = service;
        this.description = description;
        this.patId = patId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Patient getPatId() {
        return patId;
    }

    public void setPatId(Patient patId) {
        this.patId = patId;
    }

    @Override
    public String toString() {
        return "ID: "+id+" Service: "+service+" Desctiption: "+description+" Patient "+patId;
    }
    
}
