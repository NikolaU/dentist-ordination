package zubar.data;

import java.sql.Date;

public class Patient{

    private int id;
    private String firstName;
    private String lastName;
    private String pass;
    private String gender;
    private String dateOfBirth;
    private String phone;
    private String email;
    private String address;
    private Account accId;

    public Patient() {
        
    }
    
    public Patient(int id, String firstName, String lastName, String pass, String gender, String dateOfBirth, String phone, String email, String address, Account accId){
        this.setId(id);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setPass(pass);
        this.setGender(gender);
        this.setDateOfBirth(dateOfBirth);
        this.setPhone(phone);
        this.setEmail(email);
        this.setAddress(address);
        this.setAccId(accId);
    }
    
    public Patient(String firstName, String lastName, String pass, String gender, String dateOfBirth, String phone, String email, String address, Account accId){
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setPass(pass);
        this.setGender(gender);
        this.setDateOfBirth(dateOfBirth);
        this.setPhone(phone);
        this.setEmail(email);
        this.setAddress(address);
        this.setAccId(accId);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Account getAccId() {
        return accId;
    }

    public void setAccId(Account accId) {
        this.accId = accId;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    @Override
    public String toString() {
        return "Patient{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "pass: "+pass+ ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + ", phone=" + phone + ", email=" + email + ", address=" + address + ", accId=" + accId + '}';
    }
    
}