package zubar.data;

public class Doctor {

    private int id;
    private String firstName;
    private String lastName;
    private String pass;
    private String gender;
    private String phone;
    private String email;
    private String address;
    private Account accId;

    public Doctor() {
    }

    public Doctor(int id, String firstName, String lastName, String pass, String gender, String phone, String email, String address, Account accId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pass = pass;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.accId = accId;
    }
    
    public Doctor(String firstName, String lastName, String pass, String gender, String phone, String email, String address, Account accId){
        this.firstName = firstName;
        this.lastName = lastName;
        this.pass = pass;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.accId = accId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Account getAccId() {
        return accId;
    }

    public void setAccId(Account accId) {
        this.accId = accId;
    }

    @Override
    public String toString() {
        return "Doctor{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", pass=" + pass + ", gender=" + gender + ", phone=" + phone + ", email=" + email + ", address=" + address + ", accId=" + accId + '}';
    }
    
}
