package zubar.data;

import java.io.Serializable;

public class Account implements Serializable{
	
    private int id;
    private String role;
    private String description;

    public Account() {
    }

    public Account(int id, String role, String description) {
        this.setId(id);
        this.setRole(role);
        this.setDescription(description);
    }
    
    public Account(String role, String description){
        this.setRole(role);
        this.setDescription(description);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ID: "+id+" Role: "+role+" Description: "+description;
    }
    
}