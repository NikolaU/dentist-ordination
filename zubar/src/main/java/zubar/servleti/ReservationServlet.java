package zubar.servleti;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import zubar.dao.DoctorDao;
import zubar.dao.PatientDao;
import zubar.dao.ResourceManager;
import zubar.data.*;
import zubar.exception.DentistException;
import zubar.service.ReservationService;


public class ReservationServlet extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {

            List<Doctor> listDoctors = DoctorDao.getInstance().findAll(ResourceManager.getConnection());
            request.setAttribute("listDoctors", listDoctors);
            RequestDispatcher rd = request.getRequestDispatcher("rezervacija.jsp");
            rd.forward(request, response);
            
        } catch (SQLException ex) {
            Logger.getLogger(ReservationServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DentistException ex) {
            Logger.getLogger(ReservationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            String timeForm = (String) request.getParameter("choose-time");
            String dateForm = (String) request.getParameter("choose-date");
            String serviceForm = (String) request.getParameter("choose-service");
            String doctorForm = (String) request.getParameter("choose-doctor");
            int patientNumber = 0;
            HttpSession session = request.getSession();
            if(null == session.getAttribute("resUser")){
                patientNumber = 0;
                System.out.println("SESIJA JE NULLLLL");
            } else {
                patientNumber = (Integer) session.getAttribute("resUser");
                System.out.println("SESIJA NIJE NULLLLLLLLLL");
            }
            System.out.println(patientNumber);
            RequestDispatcher rd = null;
            
            DoctorDao doctorDao = DoctorDao.getInstance();
            PatientDao patientDao = PatientDao.getInstance();
            List<Doctor> listDoctors = doctorDao.findAll(ResourceManager.getConnection());
            List<Patient> listPatients = patientDao.findAll(ResourceManager.getConnection());
            Doctor doctor = null; 
            
            for(Doctor checkDoctor : listDoctors){
                String doctorName = checkDoctor.getFirstName() + " " + checkDoctor.getLastName();
                if(doctorName.equals(doctorForm)){
                    doctor = checkDoctor;
                }
            }
            
            Patient patient = null;
            if(patientNumber != 0){
                for(Patient checkPatient : listPatients){
                    if(checkPatient.getId() == patientNumber){
                        patient = checkPatient;
                    }
                }
            } else {
                rd = request.getRequestDispatcher("register.jsp");
                rd.forward(request, response);
            }
            
            Reservation reservation = new Reservation(serviceForm, dateForm, timeForm, doctor, patient);
            ReservationService reservationService = ReservationService.getInstance();
            reservationService.reserve(reservation);
            
            rd = request.getRequestDispatcher("rezervacija.jsp");
            rd.forward(request, response);
            
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReservationServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DentistException ex) {
            Logger.getLogger(ReservationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}