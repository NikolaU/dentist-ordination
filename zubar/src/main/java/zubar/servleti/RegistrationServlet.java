package zubar.servleti;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import zubar.dao.AccountDao;
import zubar.dao.PatientDao;
import zubar.dao.ResourceManager;
import zubar.data.Account;
import zubar.data.Patient;
import zubar.exception.DentistException;

public class RegistrationServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException, DentistException {

        String name = request.getParameter("namePat");
        String surname = request.getParameter("surnamePat");
        String email = request.getParameter("emailPat");
        String pass = request.getParameter("passPat");
        String gender =  request.getParameter("genderPat");
        String date = request.getParameter("datePat");
        String phone = request.getParameter("telPat");
        String address = request.getParameter("addressPat");

        Account account = AccountDao.getInstance().find(2, ResourceManager.getConnection());

        Patient patient = new Patient(name, surname, pass, gender, date, phone, email, address, account);
        int patDao = PatientDao.getInstance().insert(patient, ResourceManager.getConnection());

        System.out.println(name + " " + surname + " " + account.getId() + " " + date);

        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPut(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DentistException ex) {
            Logger.getLogger(RegistrationServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    

}
