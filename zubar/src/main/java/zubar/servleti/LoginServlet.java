package zubar.servleti;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import zubar.data.*;
import zubar.exception.DentistException;
import zubar.service.DoctorService;
import zubar.service.PatientService;

public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, DentistException, SQLException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {

            String email = request.getParameter("txtEmail");
            String pass = request.getParameter("txtPass");
            boolean checkLogin = false;
            List<Doctor> listRegisteredDoctors = DoctorService.getInstance().registeredDoctors();
            List<Patient> listRegisteredPatients = PatientService.getInstance().registeredPatients();
            RequestDispatcher rd = null;
            int idUser = 0;
            int resUser = 0;

            for (Doctor doctor : listRegisteredDoctors) {
                if (doctor.getEmail().equals(email) && doctor.getPass().equals(pass)) {
                    checkLogin = true;
                    idUser = doctor.getAccId().getId();
                    resUser = doctor.getId();
                    HttpSession session = request.getSession();
                    session.setAttribute("user", idUser);
                    System.out.println("Napravio sesiju za doktoraaaaaaaaa");
                    rd = request.getRequestDispatcher("index.jsp");
                    rd.forward(request, response);
                }
            }

            for (Patient patient : listRegisteredPatients) {
                if (patient.getEmail().equals(email) && patient.getPass().equals(pass)) {
                    checkLogin = true;
                    idUser = patient.getAccId().getId();
                    resUser = patient.getId();
                    HttpSession session = request.getSession();
                    session.setAttribute("user", idUser);
                    session.setAttribute("resUser", resUser);
                    System.out.println("Napravio sesiju za pacijentaaAAAAAAAAAAA");
                    rd = request.getRequestDispatcher("index.jsp");
                    rd.forward(request, response);
                }
            }

            if (checkLogin == false) {
                response.sendRedirect("error.jsp");
            }
            System.out.println("IDE PROCESSSSSSSSSS");
            processRequest(request, response);
        } catch (DentistException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}