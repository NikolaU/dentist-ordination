/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zubar.servleti;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import zubar.dao.ReservationDao;
import zubar.dao.ResourceManager;
import zubar.data.Reservation;
import zubar.exception.DentistException;

public class DoctorPanel extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            
            ReservationDao reservationDao = ReservationDao.getInstance();
            List<Reservation> allReservations = reservationDao.findAll(ResourceManager.getConnection());
            request.setAttribute("allReservation", allReservations);
            RequestDispatcher rd = request.getRequestDispatcher("doctor.jsp");
            rd.forward(request, response);
        
        } catch (DentistException ex) {
            Logger.getLogger(DoctorPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DoctorPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id = Integer.parseInt(request.getParameter("hiddenField"));
        System.out.println(id);
        
//        switch(id){
//            case 1: //vrate sve
//            case 2: //vrate dan
//            case 3: //vrate nedelju
//            case 4: //vrate mesec
//        }
//		if (id == 1) {
//			//vrati sve
//		}
//
//		if (id == 2) {
//			//vrati dan
//		}
//
//		if (id == 3) {
//			//vrati nedelju
//		}
//                if (id == 4) {
//			//vrati nedelju
//		}
        
    }

}
