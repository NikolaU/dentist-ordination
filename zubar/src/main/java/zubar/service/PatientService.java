package zubar.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import zubar.data.*;
import zubar.dao.*;
import zubar.exception.DentistException;

public class PatientService {
    
    private static final PatientService patientService = new PatientService();
    
    private PatientService(){}
    
    public static PatientService getInstance(){
        return patientService;
    }

    public List<Patient> registeredPatients() throws SQLException {

        Connection con = ResourceManager.getConnection();
        List<Patient> listRegisteredPatients = new ArrayList<Patient>();
        int countPatients = 0;

        try {

            List<Patient> listAllPatients = PatientDao.getInstance().findAll(con);
            for (Patient checkedPatient : listAllPatients) {
                if (checkedPatient.getAccId() != null) {
                    listRegisteredPatients.add(checkedPatient);
                    countPatients++;
                } else {
                    System.out.println("Pacijent nije registrovan na sistemu!");
                }
            }
            System.out.println(countPatients);
        } catch (SQLException ex) {
            Logger.getLogger(PatientService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DentistException ex) {
            Logger.getLogger(PatientService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listRegisteredPatients;
        
    }
}
