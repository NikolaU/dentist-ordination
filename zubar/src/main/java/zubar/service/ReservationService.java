package zubar.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import zubar.dao.ReservationDao;
import zubar.dao.ResourceManager;
import zubar.data.*;
import zubar.exception.DentistException;

public class ReservationService {
    
    private static final ReservationService reservationService = new ReservationService();

    private ReservationService() {}
    
    public static ReservationService getInstance(){
        return reservationService;
    }
    
    public void reserve(Reservation reservation) throws SQLException{
        
        Connection con = ResourceManager.getConnection();
        ReservationDao resDao = ReservationDao.getInstance();
        try {
            if(reservation != null){
                int id = resDao.insert(reservation, con);
            }
        } catch (DentistException ex) {
            Logger.getLogger(ReservationService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}