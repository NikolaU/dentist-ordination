package zubar.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import zubar.dao.DoctorDao;
import zubar.dao.ResourceManager;
import zubar.data.Doctor;
import zubar.exception.DentistException;

public class DoctorService {
    
    private static final DoctorService doctorService = new DoctorService();
    
    private DoctorService(){}
    
    public static DoctorService getInstance(){
        return doctorService;
    }
    
    public List<Doctor> registeredDoctors() throws DentistException, SQLException{
        
        List<Doctor> listRegisteredDoctors = new ArrayList<Doctor>();
        List<Doctor> listDoctors = DoctorDao.getInstance().findAll(ResourceManager.getConnection());
        
        for(Doctor doctor : listDoctors){
            if(doctor.getAccId() != null){
                listRegisteredDoctors.add(doctor);
            } else{
                System.out.println("Doktor nema account u sistemu.");
            }
        }
        
        return listRegisteredDoctors;
        
    }
    
}
