package zubar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import zubar.data.Patient;
import zubar.data.Account;
import zubar.exception.DentistException;

public class PatientDao {
    
    private static final PatientDao patientDao = new PatientDao();
    
    private PatientDao() {
    }
    
    public static PatientDao getInstance() {
        return patientDao;
    }
    
    public Patient find(int id, Connection con) throws SQLException, DentistException {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        Patient patient = null;
        Account account = null;
        
        try {
            
            String sql = "SELECT * FROM patient WHERE pat_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                account = AccountDao.getInstance().find(rs.getInt("pat_acc_id"), con);
                patient = new Patient(id, rs.getString("pat_first_name"), rs.getString("pat_last_name"), rs.getString("pat_pass"), rs.getString("pat_gender"), rs.getString("pat_date_of_birth"), rs.getString("pat_phone"), rs.getString("pat_email"), rs.getString("pat_address"), account);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }
        
        return patient;
        
    }
    
    public List<Patient> findAll(Connection con) throws SQLException, DentistException {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        Account account = null;
        Patient patient = null;
        List<Patient> listPatient = new ArrayList<Patient>();
        
        try {
            
            String sql = "SELECT * FROM patient";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                account = AccountDao.getInstance().find(rs.getInt("pat_acc_id"), con);
                patient = new Patient(rs.getInt("pat_id"), rs.getString("pat_first_name"), rs.getString("pat_last_name"), rs.getString("pat_pass"), rs.getString("pat_gender"), rs.getString("pat_date_of_birth"), rs.getString("pat_phone"), rs.getString("pat_email"), rs.getString("pat_address"), account);
                listPatient.add(patient);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }
        
        return listPatient;
        
    }
    
    public int insert(Patient patient, Connection con) throws DentistException, SQLException {
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = 0;
        
        try {
            
            String sql = "INSERT INTO patient(pat_first_name, "
                    + "pat_last_name, pat_gender, pat_date_of_birth, "
                    + "pat_phone, pat_email, pat_address, pat_acc_id, pat_pass) VALUES(?,?,?,?,?,?,?,?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, patient.getFirstName());
            ps.setString(2, patient.getLastName());
            ps.setString(3, patient.getGender());
            ps.setString(4, patient.getDateOfBirth());
            ps.setString(5, patient.getPhone());
            ps.setString(6, patient.getEmail());
            ps.setString(7, patient.getAddress());
            ps.setInt(8, patient.getAccId().getId()); //CAN BE NULL VALUE BECAUSE PATIENT DOESN'T NEED TO HAVE ACCOUNT TO BE A PATIENT
            ps.setString(9, patient.getPass());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            id = rs.getInt(1);
            
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void update(Patient patient, Connection con) throws DentistException, SQLException {
        
        PreparedStatement ps = null;
        
        try {
            
            String sql = "UPDATE patient SET pat_first_name=?, pat_last_name=?, pat_gender=?, pat_date_of_birth=?, pat_phone=?, pat_email=?, pat_address=?, pat_pass=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, patient.getFirstName());
            ps.setString(2, patient.getLastName());
            ps.setString(3, patient.getGender());
            ps.setString(4, patient.getDateOfBirth());
            ps.setString(5, patient.getPhone());
            ps.setString(6, patient.getEmail());
            ps.setString(7, patient.getAddress());
            ps.setString(8, patient.getPass());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(null, ps);
        }
        
    }
    
    public void delete(Patient patient, Connection con) throws DentistException, SQLException {
        
        PreparedStatement ps = null;
        
        try {
            
            String sql = "DELETE FROM patient WHERE pat_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, patient.getId());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(PatientDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(null, ps);
        }
        
    }
    
}
