package zubar.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import zubar.exception.DentistException;

public class ResourceManager {
    
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException{
        
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost/zubar?user=root&password=");
        return con;
    }
    
    public static void closeConnection(Connection con) throws DentistException{
        if(con!=null){
            try {
                con.close();
            } catch (SQLException ex) {
                throw new DentistException("Failed to close database connection.", ex);
            }
        }
    }
    
    public static void closeResources(ResultSet rs, PreparedStatement ps) throws SQLException{
        if(rs!=null){
            rs.close();
        }
        
        if(ps!=null){
            ps.close();
        }
    }
    
    public static void rollBackTransaction(Connection con) throws DentistException{
        if(con!=null){
            try {
                con.rollback();
            } catch (SQLException ex) {
                throw new DentistException("Failed to rollback database transactions.", ex);
            }
        }
    }
    
}
