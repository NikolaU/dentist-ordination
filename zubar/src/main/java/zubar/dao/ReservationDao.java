package zubar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import zubar.data.*;
import zubar.exception.DentistException;

public class ReservationDao {

    private static final ReservationDao reservationDao = new ReservationDao();

    private ReservationDao() {
    }

    public static ReservationDao getInstance() {
        return reservationDao;
    }

    public Reservation find(int id, Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Patient patient = null;
        Doctor doctor = null;
        Reservation reservation = null;

        try {

            String sql = "SELECT * FROM reservation WHERE res_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                patient = PatientDao.getInstance().find(rs.getInt("res_pat_id"), ResourceManager.getConnection());
                doctor = DoctorDao.getInstance().find(rs.getInt("res_doc_id"), ResourceManager.getConnection());
                reservation = new Reservation(id, rs.getString("res_service"), rs.getString("res_date"), rs.getString("res_date"), doctor, patient);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ReservationDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }

        return reservation;

    }

    public List<Reservation> findAll(Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Patient patient = null;
        Doctor doctor = null;
        Reservation reservation = null;
        List<Reservation> listReservation = new ArrayList<Reservation>();

        try {

            String sql = "SELECT * FROM reservation";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                patient = PatientDao.getInstance().find(rs.getInt("res_pat_id"), ResourceManager.getConnection());
                doctor = DoctorDao.getInstance().find(rs.getInt("res_doc_id"), ResourceManager.getConnection());
                reservation = new Reservation(rs.getInt("res_id"), rs.getString("res_service"), rs.getString("res_date"), rs.getString("res_time"), doctor, patient);
                listReservation.add(reservation);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ReservationDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }

        return listReservation;

    }

    public int insert(Reservation reservation, Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = 0;

        try {

            String sql = "INSERT INTO reservation (res_service, res_date, res_time, res_doc_id, "
                    + "res_pat_id) VALUES(?,?,?,?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, reservation.getService());
            ps.setString(2, reservation.getDate());
            ps.setString(3, reservation.getTime());
            ps.setInt(4, reservation.getDocId().getId());
            ps.setInt(5, reservation.getPatId().getId());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            id = rs.getInt(1);
            
        } catch (SQLException ex) {
            Logger.getLogger(ReservationDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(null, ps);
        }
        return id;
    }
    
    public void update(Reservation reservation, Connection con) throws DentistException, SQLException{
    
        PreparedStatement ps = null;
        
        try {
            
            String sql = "UPDATE reservation SET res_service=?, res_date=?, res_time=?, res_doc_id=?, res_pat_id=? WHERE res_id=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, reservation.getService());
            ps.setString(2, reservation.getDate());
            ps.setString(3, reservation.getTime());
            ps.setObject(4, reservation.getDocId());
            ps.setObject(5, reservation.getPatId());
            ps.setObject(6, reservation.getId());
            ps.executeUpdate();
        
        } catch (SQLException ex) {
            Logger.getLogger(ReservationDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(null, ps);
        }
        
    }
    
    public void delete(Reservation reservation, Connection con) throws DentistException, SQLException{
    
        PreparedStatement ps = null;
        
        try {

            String sql = "DELETE FROM reservation WHERE res_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, reservation.getId());
            ps.executeUpdate();
        
        } catch (SQLException ex) {
            Logger.getLogger(ReservationDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(null, ps);
        }
        
    }

}
