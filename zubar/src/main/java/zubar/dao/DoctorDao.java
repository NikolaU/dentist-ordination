package zubar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import zubar.data.Doctor;
import zubar.data.Account;
import zubar.exception.DentistException;

public class DoctorDao {
    
    private static final DoctorDao doctorDao = new DoctorDao();
    
    private DoctorDao(){}

    public static DoctorDao getInstance() {
        return doctorDao;
    }
    
    public Doctor find(int id, Connection con) throws SQLException, DentistException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        Doctor doctor = null;
        Account account = null;
        
        try {
           
            String sql = "SELECT * FROM doctor WHERE doc_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if(rs.next()){
                account = AccountDao.getInstance().find(rs.getInt("doc_acc_id"), con);
                System.out.println(account);
                doctor = new Doctor(id, rs.getString("doc_first_name"), rs.getString("doc_last_name"), rs.getString("doc_pass"), rs.getString("doc_gender"), rs.getString("doc_phone"), rs.getString("doc_email"), rs.getString("doc_address"), account);
            }
        
        } catch (SQLException ex) {
            Logger.getLogger(DoctorDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(rs, ps);
        }
        
        return doctor;
        
    }
    
    public List<Doctor> findAll(Connection con) throws SQLException, DentistException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        Doctor doctor = null;
        Account account = null;
        List<Doctor> listDoctor = new ArrayList<Doctor>();
        
        try {
            
            String sql = "SELECT * FROM doctor";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                account = AccountDao.getInstance().find(rs.getInt("doc_acc_id"), con);
                doctor = new Doctor(rs.getInt("doc_id"), rs.getString("doc_first_name"), rs.getString("doc_last_name"), rs.getString("doc_pass"), rs.getString("doc_gender"), rs.getString("doc_phone"), rs.getString("doc_email"), rs.getString("doc_address"), account);
                listDoctor.add(doctor);
            }
        
        } catch (SQLException ex) {
            Logger.getLogger(DoctorDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(rs, ps);
        }
        
        return listDoctor;
        
    }
    
    public int insert(Doctor doctor, Connection con) throws DentistException, SQLException{
    
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = 0;
        
        try {
            
            String sql = "INSERT INTO doctor (doc_first_name, doc_last_name,"
                    + "doc_pass, doc_gender, doc_phone, doc_email,"
                    + "doc_address, doc_acc_id) VALUES(?,?,?,?,?,?,?,?)";
            ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, doctor.getFirstName());
            ps.setString(2, doctor.getLastName());
            ps.setString(3, doctor.getPass());
            ps.setString(4, doctor.getGender());
            ps.setString(5, doctor.getPhone());
            ps.setString(6, doctor.getEmail());
            ps.setString(7, doctor.getAddress());
            ps.setInt(8, doctor.getAccId().getId());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            id = rs.getInt(1);
            
        } catch (SQLException ex) {
            Logger.getLogger(DoctorDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void update(Doctor doctor, Connection con) throws DentistException, SQLException{
        
        PreparedStatement ps = null;
        
        try {
          
            String sql = "UPDATE doctor SET doc_first_name=?, doc_last_name=?, doc_pass=?, doc_gender=?, doc_phone=?, doc_email=?, doc_address=?,";
            ps = con.prepareStatement(sql);
            ps.setString(1, doctor.getFirstName());
            ps.setString(2, doctor.getLastName());
            ps.setString(3, doctor.getPass());
            ps.setString(4, doctor.getGender());
            ps.setString(5, doctor.getPhone());
            ps.setString(6, doctor.getEmail());
            ps.setString(7, doctor.getAddress());
        
        } catch (SQLException ex) {
            Logger.getLogger(DoctorDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(null, ps);
        }
        
    }
    
    public void delete(Doctor doctor, Connection con) throws DentistException, SQLException{
    
        PreparedStatement ps = null;
        
        try {
            
            String sql = "DELETE FROM doctor WHERE doc_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, doctor.getId());
            ps.executeUpdate();
        
        } catch (SQLException ex) {
            Logger.getLogger(DoctorDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(null, ps);
        }
        
    }
    
}
