package zubar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import zubar.data.Patient;
import zubar.data.Record;
import zubar.exception.DentistException;

public class RecordDao {

    private static final RecordDao recordDao = new RecordDao();

    private RecordDao() {
    }

    public static RecordDao getInstance() {
        return recordDao;
    }

    public Record find(int id, Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Patient patient = null;
        Record record = null;

        try {

            String sql = "SELECT * FROM record WHERE rec_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                patient = PatientDao.getInstance().find(rs.getInt("rec_pat_id"), ResourceManager.getConnection());
                record = new Record(id, rs.getString("rec_service"), rs.getString("rec_description"), patient);
            }

        } catch (SQLException ex) {
            Logger.getLogger(RecordDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }

        return record;

    }

    public List<Record> findAll(Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Patient patient = null;
        Record record = null;
        List<Record> listRecord = new ArrayList<Record>();

        try {

            String sql = "SELECT * FROM record";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                patient = PatientDao.getInstance().find(rs.getInt("rec_pat_id"), ResourceManager.getConnection());
                record = new Record(rs.getInt("rec_id"), rs.getString("rec_service"), rs.getString("rec_description"), patient);
                listRecord.add(record);
            }

        } catch (SQLException ex) {
            Logger.getLogger(RecordDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }

        return listRecord;

    }

    public int insert(Record record, Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = 0;

        try {
            
            String sql = "INSERT INTO record VALUES(?,?,?)";
            ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, record.getService());
            ps.setString(2, record.getDescription());
            ps.setInt(3, record.getPatId().getId());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            id = rs.getInt(1);

        } catch (SQLException ex) {
            Logger.getLogger(RecordDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(null, ps);
        }
        return id;
    }
    
    public void update(Record record, Connection con) throws DentistException, SQLException{
    
        PreparedStatement ps = null;
        
        try {
            
            String sql = "UPDATE record SET rec_service=?, rec_description=?, rec_pat_id=? WHERE rec_id=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, record.getService());
            ps.setString(2, record.getDescription());
            ps.setObject(3, record.getPatId());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(RecordDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(null, ps);
        }
        
    }
    
    public void delete(Record record, Connection con) throws DentistException, SQLException{
    
        PreparedStatement ps = null;
        
        try {
            
            String sql = "DELETE FROM record WHERE rec_id=?";
            ps = con.prepareCall(sql);
            ps.setInt(1, record.getId());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(RecordDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ResourceManager.closeResources(null, ps);
        }
        
    }
    
}
