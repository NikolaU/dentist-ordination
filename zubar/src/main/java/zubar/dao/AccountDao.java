package zubar.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import zubar.data.Account;
import zubar.exception.DentistException;

public class AccountDao {

    private static final AccountDao accountDao = new AccountDao();

    private AccountDao() {
    }

    public static AccountDao getInstance() {
        return accountDao;
    }

    public Account find(int id, Connection con) throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Account account = null;

        try {

            String sql = "SELECT * FROM account WHERE acc_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                account = new Account(id, rs.getString("acc_role"), rs.getString("acc_description"));
            }

        } finally {
            ResourceManager.closeResources(rs, ps);
        }

        return account;

    }

    public List<Account> findAll(Connection con) throws SQLException, DentistException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Account account = null;
        List<Account> listAccount = new ArrayList<Account>();

        try {

            String sql = "SELECT * FROM account";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                account = new Account(rs.getInt("acc_id"), rs.getString("acc_role"), rs.getString("acc_description"));
                listAccount.add(account);
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }

        return listAccount;

    }

    public int insert(Account account, Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = 0;

        try {

            String sql = "INSERT INTO account (acc_id, acc_role, acc_description) VALUES (?,?)";
            ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, account.getRole());
            ps.setString(2, account.getDescription());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            id = rs.getInt(1);

        } catch (SQLException ex) {
            Logger.getLogger(AccountDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(rs, ps);
        }
        return id;
    }

    public void update(Account account, Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        
        try {

            String sql = "UPDATE account SET acc_role=?, acc_description=? WHERE acc_id=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, account.getRole());
            ps.setString(2, account.getDescription());
            ps.setInt(3, account.getId());
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(null, ps);
        }
    }

    public void delete(Account account, Connection con) throws DentistException, SQLException {

        PreparedStatement ps = null;
        
        try {

            String sql = "DELETE FROM account WHERE acc_id=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, account.getId());
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ResourceManager.closeResources(null, ps);
        }
    }

}
